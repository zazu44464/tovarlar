<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TovarRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TovarRepository::class)
 */
#[ApiResource]
class Tovar
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tovar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $turlari;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rangi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTovar(): ?string
    {
        return $this->tovar;
    }

    public function setTovar(string $tovar): self
    {
        $this->tovar = $tovar;

        return $this;
    }

    public function getTurlari(): ?string
    {
        return $this->turlari;
    }

    public function setTurlari(string $turlari): self
    {
        $this->turlari = $turlari;

        return $this;
    }

    public function getRangi(): ?string
    {
        return $this->rangi;
    }

    public function setRangi(string $rangi): self
    {
        $this->rangi = $rangi;

        return $this;
    }
}
