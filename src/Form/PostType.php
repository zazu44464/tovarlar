<?php

namespace App\Form;

use App\Entity\Tovar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('tovar', TextType::class, [
                'attr'=> [
                    'placeholder' => 'mahsulot nomi',
                    'class' => 'custom_class'
                ]
            ])
            ->add('turlari', TextType::class, [
                'attr'=> [
                    'placeholder' => 'mahsulot turi',
                    'class' => 'custom_class'
                ]
            ])
            ->add('rangi', TextType::class, [
                'attr'=> [
                    'placeholder' => 'mahsulot rangi',
                    'class' => 'custom_class'
                ]
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tovar::class,
        ]);
    }
}
