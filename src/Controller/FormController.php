<?php

namespace App\Controller;

use App\Entity\Tovar;
use App\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends AbstractController
{
    #[Route('/form', name: 'form')]
    public function index(Request $request): Response
    {
        $tovar = new Tovar();
        $form = $this->createForm(PostType::class, $tovar, [
            'action' => $this->generateUrl('form'),
        ]);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($tovar);
            $em->flush();
        }
        return $this->render('form/index.html.twig', [
            'post_form' => $form->createView(),
        ]);
    }
}
