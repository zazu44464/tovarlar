<?php
declare(strict_types=1);

namespace App\Component\User;


use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactory
{
    public function __construct(private UserPasswordEncoderInterface $encoder)
    {
    }
    public function create(string $email, string $password): User
    {
        $user = new User();
        $user->setEmail($email);
        $hash=$this->encoder->encodePassword($user, $password);
        $user->setPassword($hash);

        return $user;
    }

}